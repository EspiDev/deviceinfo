add_executable(update-machine-info-from-deviceinfo
    update-machine-info-from-deviceinfo.cpp
)
target_link_libraries(update-machine-info-from-deviceinfo
    deviceinfo
    PkgConfig::GLib
)
install(TARGETS update-machine-info-from-deviceinfo DESTINATION ${CMAKE_INSTALL_LIBEXECDIR})

configure_file(
    update-machine-info-from-deviceinfo.service.in
    "${CMAKE_CURRENT_BINARY_DIR}/update-machine-info-from-deviceinfo.service"
    @ONLY
)
pkg_get_variable(SYSTEMD_SYSTEM_UNIT_DIR systemd systemdsystemunitdir)
install(
    FILES "${CMAKE_CURRENT_BINARY_DIR}/update-machine-info-from-deviceinfo.service"
    DESTINATION ${SYSTEMD_SYSTEM_UNIT_DIR}
)
