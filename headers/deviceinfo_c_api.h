#ifndef DEVICEINFO_C_API_H
#define DEVICEINFO_C_API_H

// Values to use for deviceinfo_deviceType() and deviceinfo_driverType()
#define DEVICEINFO_DEVICETYPE_PHONE 0
#define DEVICEINFO_DEVICETYPE_TABLET 1
#define DEVICEINFO_DEVICETYPE_DESKTOP 2
#define DEVICEINFO_DEVICETYPE_UNKNOWN -1

#define DEVICEINFO_DRIVERTYPE_LINUX 0
#define DEVICEINFO_DRIVERTYPE_HALIUM 1

#ifdef __cplusplus
extern "C" {
#else
#include <stdbool.h>
#endif

typedef struct DeviceInfo DeviceInfo;

DeviceInfo* deviceinfo_new();
void deviceinfo_delete(DeviceInfo* instance);

char* deviceinfo_name(DeviceInfo* instance);
char* deviceinfo_prettyName(DeviceInfo* instance);
char* deviceinfo_supportedOrientations(DeviceInfo* instance);
char* deviceinfo_primaryOrientation(DeviceInfo* instance);
char* deviceinfo_portraitOrientation(DeviceInfo* instance);
char* deviceinfo_invertedPortraitOrientation(DeviceInfo* instance);
char* deviceinfo_landscapeOrientation(DeviceInfo* instance);
char* deviceinfo_invertedLandscapeOrientation(DeviceInfo* instance);
int deviceinfo_deviceType(DeviceInfo* instance);
int deviceinfo_driverType(DeviceInfo* instance);
int deviceinfo_gridUnit(DeviceInfo* instance);

char* deviceinfo_get(DeviceInfo* instance, const char* prop, const char* defaultValue);
bool deviceinfo_contains(DeviceInfo* instance, const char* prop);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // DEVICEINFO_C_API_H
